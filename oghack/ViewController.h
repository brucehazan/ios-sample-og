//
//  ViewController.h
//  oghack
//
//  Created by bruce on 5/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "FBConnect.h"

@interface ViewController : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate,FBRequestDelegate,FBSessionDelegate> {
    UIImagePickerController *picker;
    AppDelegate *appDelegate;
    UIAlertView *loading;
}
@property (weak, nonatomic) IBOutlet UIButton *fbButton;

- (IBAction)fbConnect:(id)sender;
- (IBAction)publishAction:(id)sender;

@end
