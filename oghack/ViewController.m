//
//  ViewController.m
//  oghack
//
//  Created by bruce on 5/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "AFHTTPClient.h"
#import "AFHTTPRequestOperation.h"
#import "AFJSONRequestOperation.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize fbButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if ([[appDelegate facebook] isSessionValid]) {
        [fbButton setTitle:@"Disconnect" forState:UIControlStateNormal];
    }
    loading = [[UIAlertView alloc] initWithTitle:@" " message:@" " delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
    UIActivityIndicatorView *progress= [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(125, 50, 30, 30)];
    progress.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [loading addSubview:progress];
    [progress startAnimating];    
}

- (void)viewDidUnload
{
    [self setFbButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (IBAction)fbConnect:(id)sender {
    if (![[appDelegate facebook] isSessionValid]) {
        NSArray* permissions = [[NSArray alloc] initWithObjects:
                                @"publish_actions", nil];        
        [[appDelegate facebook] authorize:permissions];
    } else {
        [[appDelegate facebook] logout];
    }
}

- (IBAction)publishAction:(id)sender {
    if ([[appDelegate facebook] isSessionValid]) {
        picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        } 
        else {
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        [self presentModalViewController:picker animated:YES];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                        message:@"Can't publish if you ain't connected"
                                                       delegate:nil
                                              cancelButtonTitle:@"Awww" 
                                              otherButtonTitles:nil];
        [alert show];         
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *) Picker {
    [Picker dismissModalViewControllerAnimated:YES];
}

- (void) imagePickerController:(UIImagePickerController *)Picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSLog(@"Selected");
    [Picker dismissModalViewControllerAnimated:YES];
    NSURL *url = [NSURL URLWithString:@"http://www.dumpyourphoto.com"];
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            @"5b141147bb490348a18227f67bf66f3cc8f5712b", @"key",
                            nil];    
    
    NSData *imageData = UIImageJPEGRepresentation([info objectForKey:@"UIImagePickerControllerOriginalImage"], 0.5);
    NSMutableURLRequest *request = 
    [httpClient 
        multipartFormRequestWithMethod:@"POST" 
        path:@"/api/upload_photo/json" 
        parameters:params  
        constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
            [formData appendPartWithFileData:imageData name:@"photo[]" fileName:@"avatar.jpg" mimeType:@"multipart/form-data"];
        }
    ];
    NSLog(@"%@", request.URL);
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSLog(@"Name: %@", [[JSON valueForKeyPath:@"sources"]valueForKeyPath:@"large"]);
//        NSMutableArray *users = [NSMutableArray arrayWithObjects:@"605665581", nil];
        NSMutableArray *users = [NSMutableArray arrayWithObjects:@"605665581", @"685145706", @"286400088", nil];
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       @"https://stark-robot-9605.herokuapp.com/beverage/latte.html", @"beverage",
                                       @"200", @"expires_in",
                                       [[JSON valueForKeyPath:@"sources"]valueForKeyPath:@"large"], @"image[0][url]",
                                       @"true", @"image[0][user_generated]",
                                       [users componentsJoinedByString:@","], @"tags",
                                       @"I wanna be drinkin' with you...", @"message",
                                       @"265781023507354", @"place",
                                       nil];    
        [[appDelegate facebook] requestWithGraphPath:@"me/ogcoffeehack:drink" andParams:params andHttpMethod:@"POST" andDelegate:self];
    } failure:^(NSURLRequest* req,NSHTTPURLResponse *req2, NSError *error,id mex) {
        NSLog(@"%@", [error description]);
    }];
    
    [loading show];
    [operation start];    
}

#pragma mark - FBSessionDelegate Methods

/**
 * Called when the user successfully logged in.
 */
- (void)fbDidLogin {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[[appDelegate facebook] accessToken] forKey:@"FBAccessTokenKey"];
    [defaults setObject:[[appDelegate facebook] expirationDate] forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
    [fbButton setTitle:@"Disconnect" forState:UIControlStateNormal];
}

/**
 * Called when the user dismissed the dialog without logging in.
 */
- (void)fbDidNotLogin:(BOOL)cancelled {
    
}

/**
 * Called after the access token was extended. If your application has any
 * references to the previous access token (for example, if your application
 * stores the previous access token in persistent storage), your application
 * should overwrite the old access token with the new one in this method.
 * See extendAccessToken for more details.
 */
- (void)fbDidExtendToken:(NSString*)accessToken
               expiresAt:(NSDate*)expiresAt {
    
}

/**
 * Called when the user logged out.
 */
- (void)fbDidLogout {
    [fbButton setTitle:@"Connect with Facebook" forState:UIControlStateNormal];
}

/**
 * Called when the current session has expired. This might happen when:
 *  - the access token expired
 *  - the app has been disabled
 *  - the user revoked the app's permissions
 *  - the user changed his or her password
 */
- (void)fbSessionInvalidated {
    
}

#pragma mark - FBRequestDelegate Methods

/**
 * Called when the Facebook API request has returned a response. This callback
 * gives you access to the raw response. It's called before
 * (void)request:(FBRequest *)request didLoad:(id)result,
 * which is passed the parsed response object.
 */
- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
    [loading dismissWithClickedButtonIndex:0 animated:YES];
}

/**
 * Called when a request returns and its response has been parsed into
 * an object. The resulting object may be a dictionary, an array, a string,
 * or a number, depending on the format of the API response. If you need access
 * to the raw response, use:
 *
 * (void)request:(FBRequest *)request
 *      didReceiveResponse:(NSURLResponse *)response
 */
- (void)request:(FBRequest *)request didLoad:(id)result {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Action Published" 
                                                    message:[NSString stringWithFormat:@"Action id: %@", [result valueForKeyPath:@"id"]]
                                                   delegate:nil
                                          cancelButtonTitle:@"Cool" 
                                          otherButtonTitles:nil];
    [alert show];    
}

/**
 * Called when an error prevents the Facebook API request from completing
 * successfully.
 */
- (void)request:(FBRequest *)request didFailWithError:(NSError *)error {
    NSLog(@"%@", [error description]);    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" 
                                                    message:@"See log for error"
                                                   delegate:nil
                                          cancelButtonTitle:@"Awww" 
                                          otherButtonTitles:nil];
    [alert show];    
}


@end
